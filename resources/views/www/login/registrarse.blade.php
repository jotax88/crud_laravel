<div class="content center-content col-12" align="center">
    <section class="first-section ">
        <div class="container ">
            <h1 class="center page-title">Registro </h1>




            <form action="{{ url('crear@registrarse') }}" class="form-full" name="registro" style="padding-bottom: 100px" method="post">

         

            <div class="col col-12 no-padding">

                <div class="rdo-inputs col col-12" >
                <div class="option-input" >

                  <label class="control control--radio">
                    <input id="persona-rdo" type="radio" name="tipoCliente" value="persona"  checked="checked" >
                    <div class=" control__indicator"></div>
                    Persona
                   </label>

                </div>
                <div class="option-input">

                  <label class="control control--radio">
                    <input id="empresa-rdo" type="radio" name="tipoCliente" value="empresa" >
                    <div class=" control__indicator"></div>
                    Empresa
                   </label>

                </div>
                  </div>


            </div><br><br>
            <div class="col col-6">
              <div class="col col-12 no-padding">
                <span id="razonSocial">Razon Social</span>

                <input type="text" name="txtRazonSocial" value="">
              </div>
              <div class="col col-12 no-padding">
                <span class="emp">Giro</span>

                <input class="emp" type="text" name="txtGiro" value="">
              </div>
              <div class="col col-12 no-padding">
                <span>


                  <div class="rdo-inputs col col-12" style="margin-top: -12px;" >
                  <div class="option-input" >

                    <label class="control control--radio">
                      <input type="radio" name="numero_identificador" value="rut"  checked="checked">
                      <div class=" control__indicator"></div>
                      RUT
                     </label>

                  </div>
                  <div class="option-input">

                    <label class="control control--radio">
                      <input type="radio" name="numero_identificador" value="dni" >
                      <div class=" control__indicator"></div>
                      DNI
                     </label>

                  </div>
                    </div>

                </span>
                </div>

                <div id="rut_input_r" class="col col-12 no-padding">
                <input  type="text" class="rut rut_validator" name="txtRut" placeholder="Rut">
                </div>
                <div id="dni_input_r" class="col col-12 no-padding">
                <input  type="text" class="dni" name="disable" placeholder="dni">
                </div>

                <div class="col col-12 no-padding">
                <span>Correo</span>

                <input type="text" name="txtCorreo">
                </div>
                <div class="col col-12 no-padding">
                <span>Contraseña</span>

                <input id="password" type="password" name="txtClave" value="">
                </div>
                <div class="col col-12 no-padding">
                <span>Repetir Contraseña</span>

                <input type="password" name="txtRepiteClave" value="">
                </div>
                <div class="col col-12 no-padding">
                <span class="persona">Fecha de nacimiento</span>

                <input class="persona date-picker" type="text" name="txtFechaNacimiento"  value="">
                </div>
            </div>
            <div class="col col-6">
              <div class="col col-12 no-padding">
                <span>Teléfono</span>

                <input type="text" name="txtTelefono" value="">
                </div>
                <div class="col col-12 no-padding">
                <span>Celular</span>

                <input type="text" name="txtCelular" value="">
                </div>

               <div class="col col-12 no-padding">
                <span>Region</span>

                <select name="cmbRegion" onChange="cargaComuna(this.value)">
                <option value="default">Seleccione Region</option>
                
                    <option value=""></option>
               
                </select>
                </div>

                <div class="col col-12 no-padding">
                <span>Comuna</span>

                <select name="cmbComuna" id="cmbComuna">
                  <option value="default">Seleccione...</option>
                </select>
                </div>
                <div class="col col-12 no-padding">

                <span>Dirección</span>

                <input type="text" name="txtDireccion" value="" >
                </div>
            </div>
            <div class="col col-12">
              <button id="btn-register" type="submit" name="button" class="right">Continuar  <i class="fa fa-caret-right" aria-hidden="true" style="float: right!important"></i></button>

            </div>

            ¿Ya tienes cuenta? <a href="">Ingresa aquí </a></p>
            </form>
        </div>
    </section>
</div>


<script>
$(document).ready(function() {

  $('#dni_input_r').hide(0);




      $('input[type=radio][name=numero_identificador]').change(function() {
          if (this.value == 'rut') {
              $('#dni_input_r').hide(0).children('.dni').attr('name', '');
              $('#rut_input_r').fadeIn(200).children('.rut').attr('name', 'txtRut').addClass('rut_validator');


          }
          else if (this.value == 'dni') {
            $('#rut_input_r').hide(0).children('.rut').attr('name', '').removeClass('rut_validator');
            $('#dni_input_r').fadeIn(200).children('.dni').attr('name', 'txtRut');
            $('#btn-register').attr('disabled',false);



          }

      });

    </script>
    
    <script >

      $.validator.addMethod("valueNotEquals", function(value, element, arg){
     return arg !== value;
    }, "Value must not equal arg.");

    $("form[name='registro']").validate({


          errorPlacement: function(label, element) {
              label.addClass('error-form-rut in-triangle');
              label.insertAfter(element);
          },
          wrapper: 'div',


    rules: {
    txtRazonSocial: "required",
    txtRut: "required",
    txtTelefono: "required",
    txtCorreo:{
      required: true,
      email: true,

    },
    txtClave: "required",
    txtRepiteClave: {
       equalTo: "#password",
    },
    txtDireccion: "required",
    cmbRegion: "required",
    cmbComuna: "required",
    cmbRegion:{valueNotEquals: "default"},
    cmbComuna:{valueNotEquals: "default"},

    },
    messages: {
    txtRazonSocial: "campo requerido",
    txtRut: "campo requerido",
    txtTelefono: "campo requerido",
    txtCorreo:{
      required: "campo requerido",
      email:"Ingresa un mail valido",
    },
    txtClave: "campo requerido",
    txtRepiteClave:"Las contraseñas no coinciden",
    txtDireccion: "campo requerido",
    cmbRegion: "campo requerido",
    cmbComuna: "campo requerido",
    cmbRegion: { valueNotEquals: "Selecciona una región" },
    cmbComuna: { valueNotEquals: "Selecciona una comuna" },

    },
    submitHandler: function(form) {
    form.submit();
    }
    });
  
    $('.persona').hide();
    $('.emp').show();
    $("#razonSocial").html('Razon Social');
  
    $('.persona').show();
    $('.emp').hide();
    $("#razonSocial").html('Nombre');
 
    $('input[type=radio][name=tipoCliente]').change(function() {
        if (this.value == 'persona') {
            $('.persona').show();
        $('.emp').hide();
      $("#razonSocial").html('Nombre');
        }
        else if (this.value == 'empresa') {
            $('.persona').hide();
        $('.emp').show();
      $("#razonSocial").html('Razon Social');
        }
    });
});


</script>
