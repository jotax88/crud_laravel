<!DOCTYPE html>
<html lang="es">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- BEGIN CSS-->
    <link rel="stylesheet" href="{{asset('css/libs_web.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- END CSS-->

  </head>

  <body>     
                          

                           
    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top" style="background-color: #ffffff!important;">
      <div class="container">
        <a class="navbar-brand" href="index.html"><img src="https://www.electroventas.cl/public/new_web/svg/logo-blue.svg" width="280" alt=""></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="about.html">VENTA EMPRESAS</a>
            </li>
             <li class="nav-item">
              <a class="nav-link" href="about.html">Llámanos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="services.html"><img src="https://www.electroventas.cl//public/new_web/svg/shopping-bag-blue.svg"  width="40" alt=""></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#" data-toggle="modal" data-target="#create"><img src="https://www.electroventas.cl//public/new_web/svg/user-blue.svg" width="40" alt=""></a>
            </li>
            
          </ul>
        </div>
      </div>
    </nav>

    <div class="modal fade" id="create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>
               
            </div>
            <div class="container">
          <h3 class="center"><strong>Inicia sesión</strong></h3>

          <form class="" action="" method="post">

              <div class="rdo-inputs col col-12" style="margin-top: -12px;">
              <div class="option-input" >

                <label class="control control--radio">
                  <input type="radio" name="ingresa_con" value="correo"  checked="checked">
                  <div class=" control__indicator"></div>
                  CORREO
                </label>

                <label class="control control--radio">
                  <input type="radio" name="ingresa_con" value="rut" >
                  <div class=" control__indicator"></div>
                  RUT
                 </label>

              </div>
              <div class="option-input">

                <label class="control control--radio">
                  <input type="radio" name="ingresa_con" value="dni" >
                  <div class=" control__indicator"></div>
                  DNI
                 </label>

              </div>
                </div>
                <div class="col col-12 no-padding">
                  Usuario
            <input type="text" id="correo_input" name="usuario" class="rut user_input" placeholder="correo" autocomplete="false" >
            <input type="text" id="rut_input" name="" class="rut user_input rut_validator-login" placeholder="Rut.." autocomplete="false" >
            <input type="text" id="dni_input" name="" class="rut user_input " placeholder="dni" autocomplete="false">
            <!-- <button class="btn btn-success">Boton prueba</button> -->
            </div>

            <div class="col col-12 no-padding">
              Contraseña
            <input type="password" name="pass" placeholder="Contraseña">
           </div>


            <button class="btn-2 center" role="submit" id="btn-login" style="margin-top: 5px!important; max-width:100%!important">Ingresar <i class="fa fa-chevron-right" aria-hidden="true" style="float:right!important"></i></button>
          </form>
          <p><a href="{{ url('crear') }}">Regístrate aquí </a></p>
          </div>
            <div class="modal-body">
                ….
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Guardar">
            </div>
        </div>
    </div>
</div>

    
    
        @yield('content')


    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

      <!-- BEGIN JS-->
    <script src="{{asset('js/libs_web.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <!-- END JS-->


  </body>

</html>

