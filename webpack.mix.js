let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

   mix.styles([
   	'resources/assets/css/libs/bootstrap.css',
   	'resources/assets/css/libs/icomoon.css',
   	'resources/assets/css/libs/flag-icon.min.css',
   	'resources/assets/css/libs/pace.css',
   	'resources/assets/css/libs/bootstrap-extended.css',
   	'resources/assets/css/libs/colors.css',
   	'resources/assets/css/libs/vertical-menu.css',
   	'resources/assets/css/libs/vertical-overlay-menu.css',
   	'resources/assets/css/libs/palette-gradient.css',
   	'resources/assets/css/libs/style.css',
   	],'public/css/libs.css');

   mix.scripts([
   	'resources/assets/js/libs/jquery.min.js',
   	'resources/assets/js/libs/tether.min.js',
   	'resources/assets/js/libs/bootstrap.min.js',
   	'resources/assets/js/libs/perfect-scrollbar.jquery.min.js',
   	'resources/assets/js/libs/unison.min.js',
   	'resources/assets/js/libs/blockUI.min.js',
   	'resources/assets/js/libs/jquery.matchHeight-min.js',
   	'resources/assets/js/libs/screenfull.min.js',
   	'resources/assets/js/libs/pace.min.js',
   	'resources/assets/js/libs/chart.min.js',
   	'resources/assets/js/libs/app-menu.js',
   	'resources/assets/js/libs/dashboard-lite.js',
   	],'public/js/libs.js');

   mix.styles([
   	'resources/assets/css/libs/bootstrap.min.css',
   	'resources/assets/css/libs/modern-business.css',
   	],'public/css/libs_web.css');

    mix.scripts([
   	'resources/assets/js/libs/jquery.min.js',
   	'resources/assets/js/libs/bootstrap.bundle.js',
   	],'public/js/libs_web.js');
/*
   mix.styles([
   	'resources/assets_web/css/libs/bootstrap.min.css',
   	'resources/assets_web/css/libs/font-awesome.min.css',
   	'resources/assets_web/css/libs/bootstrap-select.min.css',
   	'resources/assets_web/css/libs/owl.carousel.css',
   	'resources/assets_web/css/libs/owl.theme.default.css',
   	'resources/assets_web/css/libs/style.blue.css',
   	'resources/assets_web/css/libs/custom.css',
   	], 'public/css_web/libs.css');

   mix.scripts([
   	'resources/assets_web/js/libs/jquery.min.js',
   	'resources/assets_web/js/libs/popper.min.js',
   	'resources/assets_web/js/libs/bootstrap.min.js',
   	'resources/assets_web/js/libs/jquery.cookie.js',
   	'resources/assets_web/js/libs/jquery.waypoints.min.js',
   	'resources/assets_web/js/libs/jquery.counterup.min.js',
   	'resources/assets_web/js/libs/owl.carousel.min.js',
   	'resources/assets_web/js/libs/owl.carousel2.thumbs.min.js',
   	'resources/assets_web/js/libs/jquery.parallax-1.1.3.js',
   	'resources/assets_web/js/libs/bootstrap-select.min.js',
   	'resources/assets_web/js/libs/jquery.scrollTo.min.js',
   	'resources/assets_web/js/libs/front.js',
   	],'public/js_web/libs.js');*/