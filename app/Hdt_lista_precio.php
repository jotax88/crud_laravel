<?php

namespace App;
use app\Hdt_lista_precio;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_cliente
 * @property string $fecha
 * @property int $estado
 * @property int $id_forma_pago
 * @property string $nombre_lista
 */
class Hdt_lista_precio extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_cliente', 'fecha', 'estado', 'id_forma_pago', 'nombre_lista'];

}
