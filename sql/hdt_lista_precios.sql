-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-06-2018 a las 01:05:21
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `web`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hdt_lista_precios`
--

CREATE TABLE `hdt_lista_precios` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estado` int(11) NOT NULL,
  `id_forma_pago` int(11) NOT NULL,
  `nombre_lista` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `hdt_lista_precios`
--

INSERT INTO `hdt_lista_precios` (`id`, `id_cliente`, `fecha`, `estado`, `id_forma_pago`, `nombre_lista`) VALUES
(3, 44646, '2015-01-13', 0, 1, 'teclado'),
(10, 44646, '2015-01-13', 0, 1, 'super prueba'),
(13, 44646, '2015-01-16', 0, 1, 'soportes lcd'),
(17, 44646, '2015-01-27', 0, 1, 'Soporte LCD'),
(18, 44646, '2015-01-27', 0, 1, 'soportes 2'),
(28, 5373, '2015-02-04', 0, 3, 'Test 2.0'),
(31, 44646, '2015-02-10', 0, 1, 'telones electricos'),
(33, 44646, '2015-02-10', 0, 1, 'telones'),
(36, 44646, '2015-02-10', 0, 1, 'telones2'),
(37, 44646, '2015-02-11', 0, 1, 'tetones3'),
(42, 44646, '2015-02-11', 0, 1, 'soportes'),
(43, 44646, '2015-02-11', 0, 1, 'telones de proyeccion'),
(44, 44646, '2015-03-23', 0, 1, 'Soportes con Regulacion'),
(45, 55590, '2015-12-14', 1, 1, ''),
(46, 34046, '2016-01-27', 1, 1, ''),
(47, 53980, '2016-03-10', 1, 1, ''),
(48, 51868, '2016-05-12', 1, 1, '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `hdt_lista_precios`
--
ALTER TABLE `hdt_lista_precios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `hdt_lista_precios`
--
ALTER TABLE `hdt_lista_precios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
